import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get(hub, ctx, aws_apigatewayv2_api):
    ret = await hub.exec.aws.apigatewayv2.api.get(
        ctx,
        name=aws_apigatewayv2_api["name"],
        resource_id=aws_apigatewayv2_api["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_apigatewayv2_api["name"] == resource.get("name")
    assert aws_apigatewayv2_api["protocol_type"] == resource.get("protocol_type")
    assert aws_apigatewayv2_api["route_selection_expression"] == resource.get(
        "route_selection_expression"
    )
    assert aws_apigatewayv2_api["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get_invalid_resource_id(hub, ctx):
    api_get_name = "idem-test-exec-get-api-" + str(int(time.time()))
    ret = await hub.exec.aws.apigatewayv2.api.get(
        ctx,
        name=api_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.apigatewayv2.api '{api_get_name}' result is empty" in str(
        ret["comment"]
    )
