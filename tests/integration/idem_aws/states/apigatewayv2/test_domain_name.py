import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_domain_name(hub, ctx, aws_certificate_manager_import):
    domain_name = aws_certificate_manager_import["domain_name"]
    new_domain_name = "new." + domain_name
    domain_arn = aws_certificate_manager_import["resource_id"]
    domain_name_temp_name = domain_name
    endpoint_type = "REGIONAL"
    security_policy = "TLS_1_2"
    domain_name_configurations = [
        {
            "CertificateArn": domain_arn,
            "CertificateName": domain_name,
            "EndpointType": endpoint_type,
            "SecurityPolicy": security_policy,
        }
    ]
    new_domain_name_configurations = [
        {
            "CertificateArn": domain_arn,
            "CertificateName": new_domain_name,
            "EndpointType": endpoint_type,
            "SecurityPolicy": security_policy,
        }
    ]
    tags = {"idem-resource-name": domain_name_temp_name}
    new_tags = {"idem-resource-name-new": "new-" + domain_name_temp_name}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create apigatewayv2 domain name with test flag
    ret = await hub.states.aws.apigatewayv2.domain_name.present(
        test_ctx,
        name=domain_name_temp_name,
        domain_name_configurations=domain_name_configurations,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigatewayv2.domain_name", name=domain_name_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert domain_name_temp_name == resource.get("name")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    assert tags == resource.get("tags")

    # Create apigatewayv2 domain name
    ret = await hub.states.aws.apigatewayv2.domain_name.present(
        ctx,
        name=domain_name_temp_name,
        domain_name_configurations=domain_name_configurations,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigatewayv2.domain_name", name=domain_name_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert domain_name_temp_name == resource.get("name")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    assert tags == resource.get("tags")

    resource_id = resource.get("resource_id")

    # Verify that the created apigatewayv2 domain_name is present (describe)
    ret = await hub.states.aws.apigatewayv2.domain_name.describe(ctx)

    assert domain_name_temp_name in ret
    assert "aws.apigatewayv2.domain_name.present" in ret.get(domain_name_temp_name)
    resource = ret.get(domain_name_temp_name).get(
        "aws.apigatewayv2.domain_name.present"
    )
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert domain_name_temp_name == resource_map.get("name")
    assert 1 == len(resource_map.get("domain_name_configurations"))
    domain_name_configuration = resource_map.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    assert tags == resource_map.get("tags")

    # Create apigatewayv2 domain name again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.apigatewayv2.domain_name.present(
        test_ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
        domain_name_configurations=domain_name_configurations,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.domain_name",
            name=domain_name_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create apigatewayv2 domain name again with same resource_id and no change in state
    ret = await hub.states.aws.apigatewayv2.domain_name.present(
        ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
        domain_name_configurations=domain_name_configurations,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.domain_name",
            name=domain_name_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # this is to avoid a "TooManyRequestsException" thrown by the API Gateway servers
    # (request is being made too frequently and is more than what the server can handle)
    if not hub.tool.utils.is_running_localstack(ctx):
        time.sleep(30)

    # Update apigatewayv2 domain name with test flag
    ret = await hub.states.aws.apigatewayv2.domain_name.present(
        test_ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
        domain_name_configurations=new_domain_name_configurations,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.domain_name",
            name=domain_name_temp_name,
        )[0]
        in ret["comment"]
    )
    assert f"Would update parameters: domain_name_configurations" in ret["comment"]
    assert (
        f"Would update tags: Add {list(new_tags.keys())} Remove {list(tags.keys())}"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert domain_name_temp_name == resource.get("name")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert new_domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    assert new_tags == resource.get("tags")

    # Update apigatewayv2 domain name
    ret = await hub.states.aws.apigatewayv2.domain_name.present(
        ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
        domain_name_configurations=new_domain_name_configurations,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.domain_name",
            name=domain_name_temp_name,
        )[0]
        in ret["comment"]
    )
    assert f"Updated parameters: domain_name_configurations" in ret["comment"]
    assert (
        f"Updated tags: Add {list(new_tags.keys())} Remove {list(tags.keys())}"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert domain_name_temp_name == resource.get("name")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert new_domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Search for existing apigatewayv2 domain name
    ret = await hub.states.aws.apigatewayv2.domain_name.search(
        ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert domain_name_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert new_domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Search for non-existing apigatewayv2 domain name
    fake_resource_id = "fake-domain-" + str(int(time.time()))
    ret = await hub.states.aws.apigatewayv2.domain_name.search(
        ctx,
        name=domain_name_temp_name,
        resource_id=fake_resource_id,
    )

    assert not ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.apigatewayv2.domain_name resource with resource id '{fake_resource_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Delete apigatewayv2 domain name with test flag
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        test_ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigatewayv2.domain_name", name=domain_name_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert domain_name_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert new_domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Delete apigatewayv2 domain name
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigatewayv2.domain_name", name=domain_name_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert domain_name_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert domain_arn == domain_name_configuration.get("CertificateArn")
    assert new_domain_name == domain_name_configuration.get("CertificateName")
    assert endpoint_type == domain_name_configuration.get("EndpointType")
    assert security_policy == domain_name_configuration.get("SecurityPolicy")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Delete the same apigatewayv2 domain name again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        ctx,
        name=domain_name_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.domain_name", name=domain_name_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete apigatewayv2 domain name with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        ctx,
        name=domain_name_temp_name,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.domain_name", name=domain_name_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
