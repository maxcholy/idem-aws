import copy
import time
import uuid

import pytest


@pytest.mark.asyncio
async def test_role_policy(hub, ctx, aws_iam_role, aws_iam_role_2):
    role_policy_temp_name = "idem-test-role-policy-" + str(int(time.time()))
    role_name = aws_iam_role.get("name")
    policy_document = '{"Statement": [{"Action": ["ec2:DescribeTags"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'

    # Create IAM role policy with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.role_policy.present(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_policy_temp_name == resource.get("name")
    assert resource.get("resource_id", None), "Fake resource id for 'testing'"
    assert "Would create" in str(ret["comment"])

    # Create IAM role policy
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_policy_temp_name == resource.get("name")
    resource_id = resource.get("resource_id")
    assert resource_id is not None, "resource_id expected"

    # Create IAM role policy - different role same policy
    role_name_2 = aws_iam_role_2.get("name")
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_policy_temp_name == resource.get("name")
    resource_id_2 = resource.get("resource_id")
    assert resource_id_2 is not None, "resource_id expected"

    new_policy_document = '{"Statement": [{"Action": ["ec2:ListTags"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'

    # Update policy with invalid resource_id with test flag
    ret = await hub.states.aws.iam.role_policy.present(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        policy_document=new_policy_document,
        resource_id="invalid-resource_id",
    )
    assert not ret["result"], ret["comment"]
    assert "Incorrect aws.iam.role_policy resource_id: invalid-resource_id." in str(
        ret["comment"]
    )

    # Update policy with test flag
    ret = await hub.states.aws.iam.role_policy.present(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        policy_document=new_policy_document,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret.get("new_state").get("policy_document") == new_policy_document
    assert "Would update" in str(ret["comment"])
    assert "policy_document" in str(ret["comment"])

    # Update policy document for real on second role
    # Since it is an embedded policy it should not be modified
    # for the first role, even if has the same name
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        policy_document=new_policy_document,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret.get("new_state").get("policy_document") == new_policy_document
    assert "Updated" in str(ret["comment"])
    assert "policy_document" in str(ret["comment"])

    # Describe -> should have the two role policies, even though the policy name is the same
    describe_ret = await hub.states.aws.iam.role_policy.describe(ctx)
    resource_key_1 = f"iam-role-policy-{resource_id}"
    resource_key_2 = f"iam-role-policy-{resource_id_2}"
    assert resource_key_1 in describe_ret
    assert resource_key_2 in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret.get(resource_key_1)["aws.iam.role_policy.present"],
        "name",
        role_policy_temp_name,
    )
    hub.tool.utils.verify_in_list(
        describe_ret.get(resource_key_2)["aws.iam.role_policy.present"],
        "name",
        role_policy_temp_name,
    )
    # Embedded policies are different, even if named the same
    hub.tool.utils.verify_in_list(
        describe_ret.get(resource_key_1)["aws.iam.role_policy.present"],
        "policy_document",
        policy_document,
    )
    hub.tool.utils.verify_in_list(
        describe_ret.get(resource_key_2)["aws.iam.role_policy.present"],
        "policy_document",
        new_policy_document,
    )

    # Create again
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        resource_id=resource_id,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "already exists" in str(ret["comment"]), ret["comment"]

    # Delete IAM role policy with invalid resource_id with test flag
    ret = await hub.states.aws.iam.role_policy.absent(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        resource_id="invalid-resource_id",
    )
    assert not ret["result"], ret["comment"]
    assert "Incorrect aws.iam.role_policy resource_id: invalid-resource_id." in str(
        ret["comment"]
    )

    # Delete IAM role policy with test flag
    ret = await hub.states.aws.iam.role_policy.absent(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert "Would delete" in str(ret["comment"]), ret["comment"]

    # Delete IAM role policy
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx, name=role_policy_temp_name, role_name=role_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete second IAM role policy
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        resource_id=resource_id_2,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM role policy again
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx, name=role_policy_temp_name, role_name=role_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")


@pytest.mark.asyncio
async def test_role_policy_absent_with_none_role_name(hub, ctx):
    policy_name = "idem-test-role-policy-" + str(uuid.uuid4())
    # Delete role_policy with role_name as None. Result in no-op.
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx, name=policy_name, role_name=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.iam.role_policy", name=policy_name
        )[0]
        in ret["comment"]
    )
