import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_subscription_filter(
    hub,
    ctx,
    aws_lambda_function,
    aws_cloudwatch_log_group,
    aws_lambda_function_permission,
):
    name = "idem-test-subscription-filter-" + str(uuid.uuid4())
    log_group_name = aws_cloudwatch_log_group.get("resource_id")
    filter_pattern = "ERROR"
    destination_arn = aws_lambda_function.get("function_arn")

    # Create subscription_filter with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.logs.subscription_filter.present(
        test_ctx,
        name=name,
        log_group_name=log_group_name,
        filter_pattern=filter_pattern,
        destination_arn=destination_arn,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert name == resource.get("name")
    assert log_group_name == resource.get("log_group_name")
    assert filter_pattern == resource.get("filter_pattern")
    assert destination_arn == resource.get("destination_arn")

    # Create subscription_filter in real
    ret = await hub.states.aws.logs.subscription_filter.present(
        ctx,
        name=name,
        log_group_name=log_group_name,
        filter_pattern=filter_pattern,
        destination_arn=destination_arn,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert name == resource.get("name")
    assert log_group_name == resource.get("log_group_name")
    assert filter_pattern == resource.get("filter_pattern")
    assert destination_arn == resource.get("destination_arn")

    # Describe subscription_filter in localstack
    # Each log_group can have upto 2 subscription_filter and there can be more than 10,000 log_groups in aws.
    # Due to it being time intensive running describe on localstack only.
    if hub.tool.utils.is_running_localstack(ctx):
        describe_ret = await hub.states.aws.logs.subscription_filter.describe(ctx)
        assert resource_id in describe_ret
        assert "aws.logs.subscription_filter.present" in describe_ret.get(resource_id)
        described_resource = describe_ret.get(resource_id).get(
            "aws.logs.subscription_filter.present"
        )
        described_resource_map = dict(ChainMap(*described_resource))
        assert name == described_resource_map.get("name")
        assert log_group_name == described_resource_map.get("log_group_name")
        assert filter_pattern == described_resource_map.get("filter_pattern")
        assert destination_arn == described_resource_map.get("destination_arn")

    # Update subscription_filter with test flag
    filter_pattern = "PASS"
    ret = await hub.states.aws.logs.subscription_filter.present(
        test_ctx,
        name=name,
        log_group_name=log_group_name,
        filter_pattern=filter_pattern,
        destination_arn=destination_arn,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_update_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert name == resource.get("name")
    assert log_group_name == resource.get("log_group_name")
    assert filter_pattern == resource.get("filter_pattern")
    assert destination_arn == resource.get("destination_arn")

    # Update subscription_filter in real
    ret = await hub.states.aws.logs.subscription_filter.present(
        ctx,
        name=name,
        log_group_name=log_group_name,
        filter_pattern=filter_pattern,
        destination_arn=destination_arn,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.update_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert name == resource.get("name")
    assert log_group_name == resource.get("log_group_name")
    assert filter_pattern == resource.get("filter_pattern")
    assert destination_arn == resource.get("destination_arn")

    # Delete subscription_filter with test flag
    ret = await hub.states.aws.logs.subscription_filter.absent(
        test_ctx,
        name=name,
        log_group_name=log_group_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete subscription_filter in real
    ret = await hub.states.aws.logs.subscription_filter.absent(
        ctx,
        name=name,
        log_group_name=log_group_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.logs.subscription_filter.absent(
        ctx,
        name=name,
        log_group_name=log_group_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.logs.subscription_filter", name=name
        )[0]
        in ret["comment"]
    )
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
